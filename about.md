---
layout: page
title: About
permalink: /about/
---

StarOpenSource is a one-man team and develops free and open source software. This page contains up to date information about the current StarOpenSource data partition incident. \
 \
You can view the source for this page [here](https://gitlab.com/staropensource/staropensource-recovery/).
