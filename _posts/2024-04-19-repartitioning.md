---
layout: post
title:  "Repartitioning"
date:   2024-04-19 15:02:00 +0200
---
I am currently repartitioning the hard disk, including the system partition. \
Why you might ask? Because the system and data partitions are formatted as btrfs and btrfs recovery tools aren't as widely available as there are for ext4 for example. Also I don't really understand btrfs, I still don't understand why I chose btrfs for the system and data partitions to this day. \
Anyway, I'm copying all files off the system partition onto a temporary one at the moment. Once that's done I'll format the system partition and copy all files back over, with all metadata of course. \
After that's done I'll clean the temporary partition, copy some useful files and directories off the data partition and format it. \
