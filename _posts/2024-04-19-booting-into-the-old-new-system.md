---
layout: post
title:  "Booting into the old new system"
date:   2024-04-19 16:11:00 +0200
---
I've successfully booted into the server system!
The copying and repartitioning processes finished around half an hour ago, but I needed to fix fstab and a few other things before the system booted. \
Anyway, I'm in now and can ssh into the server and reconfigure everything... great.
