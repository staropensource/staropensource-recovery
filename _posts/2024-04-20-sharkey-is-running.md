---
layout: post
title:  "Sharkey is running!"
date:   2024-04-20 10:45:00 +0200
---
Our Sharkey instance is now online again! I needed to bootstrap and setup a new instance unfortunately (as stated in the last update) as it would break federation otherwise. \
I've already imported all emojis, updated the instance settings and created StarOpenSource's accounts and my own one. Not only that, but I also added relays and increased Sharkey's worker count so it federates faster (it's federating w/ 630 instances already!).

---

 \
Anyway, now that sos!fly (I still find that new name funny) is online I want to shift my focus to all other StarOpenSource services, those being Piped, Grafana, Shlink (our link shortener) and the PHP server with all of it's web services. \
And before I forget it: StarOpenSource now uses Valkey as it's Redis replacement. Fuck source available and Redis.
