---
layout: post
title:  "We lost our data partition"
date:   2024-04-19 13:10:40 +0200
---
Yesterday I (JeremyStarTM) performed some maintenance on StarOpenSource's infrastructure, this time it was the web server.
I wanted to try out freenginx, but I didn't want to use community docker images because they are often abandoned after a few weeks or months.
So I build an image myself. During that time he chatted with another guy and he suggested to use lighthttpd instead (docker images exist for lighttpd).
Following that suggestion, I tried deleting `/data/dockerbuild`, which only contained that Dockerfile to build a freenginx image.
But instead of entering `rm -rf /data/dockerbuild` I entered `rm -rf /data/docker` and pressed tab, directly followed by enter without checking what I was doing.
Only a second later, I realized my mistake: I accidentally removed almost all data on the StarOpenSource server (which was located in `/data/docker`).

---

 \
Now you might think "You must have backups right?"... the answer is no. Backups add more to the monthly server costs on Hetzner which I can't afford, plus manual backups waste time and I didn't think that a desaster like this would happen. Well, wrong.

---

 \
Anyway, I'm currently trying to recover as much data as possible. I've already taken a backup of the server disk and stored it on my local machine. I've also setup this "blog" to more easily inform about updates on this incident.
