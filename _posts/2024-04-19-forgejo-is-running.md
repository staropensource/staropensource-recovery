---
layout: post
title:  "Forgejo is running!"
date:   2024-04-19 16:11:00 +0200
---
sos!git (our Forgejo instance) is now back online again! I've imported all repositories, added all labels and organizations and ran healthchecks over all repos. AFAIK no repository has been damaged, which is good. Only a few issues were lost, but that's not really a problem since I created them all and have them in my head.

---

 \
Anyway, I've just looked at our Sharkey instance and will take it online in a few seconds (because not much reconfiguration is needed for Sharkey). I have to change the domain name though otherwise federation breaks (unfortunately). One of my friends suggested **fly** as the new service & instance name (because it's first of all similar to sky and it's funny).
