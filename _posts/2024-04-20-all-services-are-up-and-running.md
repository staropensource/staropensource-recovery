---
layout: post
title:  "All services are up and running"
date:   2024-04-20 14:04:00 +0200
---
All StarOpenSource services are back online again! \
As mentioned before because the databases were lost our Forgejo, Sharkey and Piped instances all suffered complete data lost. \
But now that all services are running again, I need to think of plans on how to make sure that this catastrophic event never happens again. I'll create a new post on here when I've come up with some ideas.
