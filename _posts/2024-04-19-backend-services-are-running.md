---
layout: post
title:  "Backend services are running"
date:   2024-04-19 16:11:00 +0200
---
I've been working at at bringing all backend services (databases, our email server, etc.) back online again.
The most time consuming service was the email server as email is a bit complex to setup. Fortunately I've gone through that rodeo one time already.
Anyway, now I can focus on bringing the StarOpenSource's services back online, starting with Uptime Kuma and then Forgejo... wish me luck.
