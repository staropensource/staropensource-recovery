# StarOpenSource Recovery Website
This website serves up to date information about StarOpenSource's current server incident.

# [Visit](https://recovery.staropensource.de)

# CI/CD
[![Build Status](https://gitlab.com/staropensource/staropensource-recovery/master/pipeline.svg)](https://gitlab.com/staropensource/staropensource-recovery/-/pipelines?ref=master)
